import {Server} from "./Server";
import express from 'express';
import dotenv from 'dotenv';
import Mongoose from './db/Mongoose';

const app = express();

dotenv.config();

Mongoose.getInstance();

const port = process.env.PORT || '8080';

const server = new Server(app);

server.start(port);