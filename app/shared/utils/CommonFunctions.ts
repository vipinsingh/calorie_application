import { ERRORS } from './../../lang/en/error_messages';
import { NODE_ENV } from './../../config/config';
import bcrypt from 'bcrypt';
import { Request, Response } from 'express';

export class CommonFunctions {
    
    static filterObj(obj, filterArr) {
        const newObj = {}
        filterArr.forEach(el => {
            if(obj[el] !== undefined) {
                newObj[el] = obj[el];
            }
        });

        return newObj;
    }

    static encrypt(str : string) {
        const encryptedStr = bcrypt.hashSync(str, 10);

        return encryptedStr;
    }

    static verifyPassword(password : string, hash : string) {
        return bcrypt.compareSync(password, hash);
    }

    static generateSessionToken() {
        let str = '';
        for(let i = 0; i< 3; i++) {
            str+=(Math.random().toString(36).substring(2, 36) + Math.random().toString(36).substring(2, 15));
        }
        return str;
    }

    static createSuccessResponse(req : Request, res : Response, data : object | null = null, code : number = 200) {
        let response = {
            status : 'success',
            code :  code,
            data
        };

        return res.status(code).json(response);
    }


    static createErrorResponse(req : Request, res: Response, errors : object | any | null = null, code = 500) {
        let response : any = {
            status : 'failed',
            code : code
        }

        if(code === 500) {
            if(NODE_ENV === 'dev') {
                response.errors = String(errors.stack ? errors.stack : errors);
            } else {
                response.errors = ERRORS['action_failed'];
            }
        } else {
            response.errors = errors;
        }

        return res.status(code).json(response);
    }
}