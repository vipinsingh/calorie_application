import UserService from "../../modules/User/services/UserService";
import { CommonFunctions } from "../utils/CommonFunctions";
import { ERRORS } from '../../lang/en/error_messages';
import { Request, Response,NextFunction } from "express";

export default class Authorization {

                static auth(req:Request, res:Response, next:NextFunction) {
                // Get token from header
                const token = req.header('Authorization');
                
                let user_Id = null;

                const params = {};

                if(req.body.user_id){
                    user_Id = req.body.user_id;
                }else{
                    user_Id = req.query.user_id;  
                }

                // Check if not token
                if (!token) {
                    return CommonFunctions.createErrorResponse(req, res, ERRORS['authorization_failed'],401);
                }

                
                params["user_id"] = user_Id;
                params["token"] = token;

                UserService.checkAuthStatus(params)
                .then(data => {
                    if(!data)
                    return CommonFunctions.createErrorResponse(req, res, ERRORS['authorization_failed'],401);

                    next();
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, ERRORS['authorization_failed'],401);
                })

                };

}
