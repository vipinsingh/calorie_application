import {Router} from "express";

export default class ApiRouter {
    private static instance : Router

    constructor() {}

    get instance() {
        ApiRouter.instance = Router();
        return ApiRouter.instance;
    }
}