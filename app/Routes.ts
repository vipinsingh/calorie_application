import ApiRouter from "./shared/ApiRouter/ApiRouter";
import UserRouter from './modules/User/routes/UserRouter';
import CalorieRouter from './modules/Calorie/routes/CalorieRouter';
import {Router} from "express";


class Routes extends ApiRouter{
    routes : Router

    constructor() {
        super();
        this.routes = this.instance;
        this.setRoutes();
    }

    setRoutes() {
        this.routes.use('/calorie', CalorieRouter);
        this.routes.use('/user', UserRouter);
    }
}

export default new Routes().routes;