export const FIELDS = {
    signup         : ['name', 'email', 'password'],
    signin         : ['email', 'password'],
    logout         : ['user_id', 'token'],
    auth_status    : ['user_id', 'token'],
    calorie        : ['meal','user_id','calorie'],
    del_update_calorie  : ['_id'],
    password             : 'password',
    email                : 'email',
    mongo_id             : '_id',
    client               : 'client',
    name                 : 'name',
    user_id              : 'user_id',
    project_id           : 'project_id',
    token                : 'token',
    page_no              : 'page_no',
    per_page             : 'per_page',
    id                   : 'id',
    return_user_signup : [
        'id',
        'name',
        'email',
        'created_at',
        'updated_at'
    ]
}