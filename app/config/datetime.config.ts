class DateTime {

    getDateUnixTimestamp() {
        return Math.floor(Date.now() / 1000);
    }

    convertTimestampToIsoDate(timestamp: any) {
        return new Date(timestamp * 1000).toISOString();
    }
}

export default new DateTime();
