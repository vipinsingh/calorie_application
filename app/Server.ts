// import { LogRequest } from './shared/middlewares/LogRequest';
import {Express, Request, Response} from "express";
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import Routes from './Routes';
import cors from 'cors';
import { CommonFunctions } from "./shared/utils/CommonFunctions";
import {ERRORS} from "./lang/en/error_messages"

export class Server {

    private app: Express;

    constructor(app: Express) {
        this.app = app;

        this.app.use(express.static(path.resolve("./") + "/frontend/build"));

        this.app.use(cors());

        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
      
        // this.app.use((req,res,next)=>{console.log("req url ----",req.url); next()})
        this.app.use("/api/v1", Routes);

        this.app.get("*", (req: Request, res: Response): void => {
            res.sendFile(path.resolve("./") + "/frontend/build/index.html");
        });
        
        this.app.use((req: any, res: any, next: any) => {
            const errorObject = CommonFunctions.createErrorResponse(req,res, {message:ERRORS.resource_not_found.message,type:ERRORS.resource_not_found.type}, ERRORS.resource_not_found.status);
            next(errorObject);
            return;
        })
    }

    public start(port: string): void {
        this.app.listen(parseInt(port), () => console.log(`Server listening on port ${port}!`));
    }

}