import { SchemaBuilder } from './../../../db/SchemaBuilder';

class Session extends SchemaBuilder {

    constructor() {
        super({
            user_id: {
                type: String
            },
            is_active: {
                type: Boolean,
                default : true
            },
            session_token:  {
                type : String
            }
        }, 'session_logs');
    }
}

export default new Session().Model;