import { SchemaBuilder } from './../../../db/SchemaBuilder';

class User extends SchemaBuilder {

    constructor() {
        super({
            name: {
                type: String
            },
            email: {
                type: String
            },
            password: {
                type: String
            }
        }, 'user');

        /**
         * @description Find a user for given email
         * 
         * @author Tanuj Chawla <tanuj.chawla@biz2credit.com>
         * 
         * @param {string} email
         * 
         * @returns Promise
         */
        this.schema.statics.findUserByEmail = function (email) {
            const User = this;
            
            return User.findOne({email : email})
                    .then((user) => {
                        return Promise.resolve(user);
                    });

        }
    }
} 

export default new User().Model;