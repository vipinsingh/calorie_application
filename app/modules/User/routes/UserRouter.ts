import { SIGNUP, SIGNIN, LOGOUT, CHECK_AUTH_STATUS } from '../validators/userValidationConfig';
import { RequestValidator } from './../../../shared/middlewares/RequestValidator';
import { ValidationErrorHandler } from './../../../shared/middlewares/ValidationErrorHandler';
import { Router } from "express";
import UserController from './../controllers/UserController';
import ApiRouter from '../../../shared/ApiRouter/ApiRouter';

class UserRouter extends ApiRouter {
    router : Router;

    constructor() {
        super();
        this.router = this.instance;
        this.setUserRoutes();
    }

    setUserRoutes() {
        this.router.post('/signup', RequestValidator.validateReq(SIGNUP), ValidationErrorHandler.checkError, UserController.signup);
        this.router.post('/signin', RequestValidator.validateReq(SIGNIN), ValidationErrorHandler.checkError, UserController.signin);
        this.router.post('/logout', RequestValidator.validateReq(LOGOUT), ValidationErrorHandler.checkError, UserController.logout);
        this.router.post('/auth_status', RequestValidator.validateReq(CHECK_AUTH_STATUS), ValidationErrorHandler.checkError, UserController.checkAuthStatus);
    }
}

export default new UserRouter().router;