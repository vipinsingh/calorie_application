// import { CommonFunctions } from './../../../shared/utils/CommonFunctions';
// import { FIELDS } from './../../../config/fields';
import { QueryBuilderService } from './../../../shared/services/QueryBuilderService';
import Calorie from './../models/Calorie';

class CalorieService {
    private queryBuilder : QueryBuilderService;

    constructor(queryBuilder : QueryBuilderService) {
        this.queryBuilder = queryBuilder;
    }

    getCalorie(calorieData:any){
        return this.queryBuilder.Find('Calorie', calorieData)
            .then(dataToReturn => {
                return Promise.resolve(dataToReturn);
            })
    }

    totalCalories(calories){
        let sum = 0;
        for(let i=0;i<calories.length;i++){
            sum = sum + calories[i];
        }
        return sum;
    }

    getCalorieById(calorieData:any){
        return this.queryBuilder.FindOne('Calorie', calorieData)
            .then(dataToReturn => {
                return Promise.resolve(dataToReturn);
            })
    }

    createCalorie(calorieData:any){        
        return this.queryBuilder.Create('Calorie', calorieData)
        .then(dataToReturn => {
            return Promise.resolve(dataToReturn);
        })
    }

    deleteCalorie(calorieData:any){
        return this.queryBuilder.deleteMany('Calorie',calorieData)
        .then(dataToReturn => {
            return Promise.resolve(dataToReturn);
        })
    }

    updateCalorie(calorieParams,calorieData:any){
        return this.queryBuilder.Update('Calorie',calorieParams,calorieData)
        .then(dataToReturn => {            
            return Promise.resolve(dataToReturn);
        })
    }



   
}

export default new CalorieService(new QueryBuilderService());