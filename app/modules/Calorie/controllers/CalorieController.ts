import { Request, Response } from "express";
import { CommonFunctions } from "../../../shared/utils/CommonFunctions";
import { FIELDS } from "../../../config/fields";
import CalorieService from "../services/CalorieService";

class CalorieController {
    getCalorie(req : Request, res : Response) {
        try{        
            CalorieService.getCalorie(req.query)
                .then(calorie => {
                    return CommonFunctions.createSuccessResponse(req, res, calorie, 200);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                });
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    getCalorieById(req : Request, res : Response) {
        try {
            CalorieService.getCalorieById(req.params)
            .then(calorie => {
                return CommonFunctions.createSuccessResponse(req, res, calorie, 200);
            })
            .catch(error => {
                return CommonFunctions.createErrorResponse(req, res, error);
            });
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    createCalorie(req : Request, res : Response) {
        try{
            const calorieData = CommonFunctions.filterObj(req.body, FIELDS.calorie);
                        
            CalorieService.createCalorie(calorieData)
                .then(calorie => {                    
                    return CommonFunctions.createSuccessResponse(req, res, calorie, 201);
                })
                .catch(error => {                    
                    return CommonFunctions.createErrorResponse(req, res, error);
                });
        } catch (error) {            
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    updateCalorie(req : Request, res : Response) {
        try{
            const calorieData = CommonFunctions.filterObj(req.body, FIELDS.calorie);     
            const paramsData = CommonFunctions.filterObj(req.params, FIELDS.del_update_calorie);          
            CalorieService.updateCalorie(paramsData,calorieData)
                .then(calorie => {
                    return CommonFunctions.createSuccessResponse(req, res, calorie, 200);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                });
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }

    deleteCalorie(req : Request, res : Response){
        try {
            const calorieData = CommonFunctions.filterObj(req.params, FIELDS.del_update_calorie);    
                    
            CalorieService.deleteCalorie(calorieData)
                .then(calorie => {
                    return CommonFunctions.createSuccessResponse(req, res, calorie, 200);
                })
                .catch(error => {
                    return CommonFunctions.createErrorResponse(req, res, error);
                });
        } catch (error) {
            return CommonFunctions.createErrorResponse(req, res, error);
        }
    }
}

export default new CalorieController();