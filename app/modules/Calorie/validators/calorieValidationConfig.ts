export const CREATECALORIE = [
    {
        name : 'meal',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'user_id',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    },
    {
        name : 'calorie',
        paramIn : 'body',
        validations : [
            {
                name : 'required',
                message : 'required',
                bail  : true
            },
            {
                name : 'notEmpty',
                message : 'required',
                bail  : true
            }
        ]
    }
];