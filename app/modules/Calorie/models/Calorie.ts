import { SchemaBuilder } from './../../../db/SchemaBuilder';

class Calorie extends SchemaBuilder {

    constructor() {
        super({
            user_id: {
                type: String
            },
            calorie:{
                type:Number
            },
            meal:{
                type:String
            }
            
        }, 'calorie');
    }
}

export default new Calorie().Model;