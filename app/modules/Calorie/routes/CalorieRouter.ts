import { RequestValidator } from './../../../shared/middlewares/RequestValidator';
import { ValidationErrorHandler } from './../../../shared/middlewares/ValidationErrorHandler';
import { Router } from "express";
import CalorieController from './../controllers/CalorieController';
import ApiRouter from '../../../shared/ApiRouter/ApiRouter';
import { CREATECALORIE } from '../validators/calorieValidationConfig';

class CalorieRouter extends ApiRouter {
    router : Router;

    constructor() {
        super();
        this.router = this.instance;
        this.setUserRoutes();
    }

    setUserRoutes() {
        this.router.get('/', CalorieController.getCalorie);
        this.router.get('/:_id', CalorieController.getCalorieById);
        this.router.post('/', RequestValidator.validateReq(CREATECALORIE), ValidationErrorHandler.checkError,CalorieController.createCalorie);
        this.router.patch('/:_id',CalorieController.updateCalorie);
        this.router.delete('/:_id',CalorieController.deleteCalorie);
    }
}

export default new CalorieRouter().router;