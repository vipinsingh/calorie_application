export const ERRORS = {
    invalid : {
        type : 'FIELD_INVALID',
        message: 'The value of the field is invalid.'
    },
    invalid_string :  {
        type : 'FIELD_INVALID',
        message: 'The value of the field must be a valid string.'
    },
    invalid_number :  {
        type : 'FIELD_INVALID',
        message: 'The value of the field must be a valid number.'
    },    
    duplicate : {
        type : 'FIELD_DUPLICATE',
        message : 'The value of the field is already used for another resource.'
    },
    invalid_boolean :  {
        type : 'FIELD_INVALID',
        message: 'The value of the field must be boolean.'
    },
    required : {
        type : 'FIELD_REQUIRED',
        message: 'This action requires the field to be specified.'
    },
    action_failed : {
        type : 'ACTION_FAILED',
        message :  'The server failed to perform this action for unknown internal reason.'
    },
    authentication_failed : {
        type   : 'AUTHENTICATION_FAILED',
        message: 'Used authentication credentials are invalid.'
    },
    resource_not_found : {
        type   : 'RESOURCE_NOT_FOUND',
        message: 'Resource does not exist or has been removed.',
        status: 400
    }
};