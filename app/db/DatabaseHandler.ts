import Calorie from './../modules/Calorie/models/Calorie';
import Session from './../modules/User/models/Session';
import User from './../modules/User/models/User';
import Mongoose from './Mongoose';

export class DatabaseHandler {
    private static instance : DatabaseHandler

    private _models;

    constructor() {
        Mongoose.getInstance();

        this._models = {
            User          : User,
            Session       : Session,
            Calorie       : Calorie
        }
    }

    public static get Models()  {
        if (!DatabaseHandler.instance) {
            DatabaseHandler.instance = new DatabaseHandler();
        }
        return DatabaseHandler.instance._models;

    }
}