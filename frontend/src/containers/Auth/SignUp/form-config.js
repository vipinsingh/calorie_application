export const controls = {
    name :  {
        elementType : 'input',
        elementConfig : {
            type : 'text',
            placeholder : 'Your Name'
        },
        value : '',
        validation : {
            required : true,
            minLength : 2
        },
        valid : false,
        touched : false
    },
    email :  {
        elementType : 'input',
        elementConfig : {
            type : 'email',
            placeholder : 'E-mail Address'
        },
        value : '',
        validation : {
            required : true,
            isEmail : true  
        },
        valid : false,
        touched : false
    },
    password :  {
        elementType : 'input',
        elementConfig : {
            type : 'password',
            placeholder : 'Password'
        },
        value : '',
        validation : {
            required : true,
            minLength : 6 
        },
        valid : false,
        touched : false
    }
};