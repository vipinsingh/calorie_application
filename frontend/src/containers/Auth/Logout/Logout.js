import React, { Component } from 'react';
import * as actions from '../../../store/actions/index';
import {toast} from 'react-toastify';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Aux from '../../../hoc/Auxillary/Auxillary';


class Logout extends Component {

    componentDidMount() {
        this.props.onLogout();
    }

    render() {
        if(this.props.error) {
            toast.error('An error occured!');
            this.props.onErrorReset();
        }

        let authRedirect = null;
        if(!this.props.isAuth) {
            authRedirect = <Redirect to="/" />
        }

        const spinner = this.props.loading ? <Spinner /> : null;
        
        return (
            <Aux>
                {spinner}
                {authRedirect}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuth : state.auth.token !== null,
        error : state.auth.error,
        loading : state.auth.loading
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogout : () => dispatch(actions.authLogout()),
        onErrorReset : () => dispatch(actions.resetAuth())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout);