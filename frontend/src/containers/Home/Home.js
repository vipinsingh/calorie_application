import React, { Component } from 'react';
import NavigationBar from './NavigationBar/NavigationBar';
import { Route, Switch, Redirect } from 'react-router-dom';
import Calorie from './Calorie/Calorie'

class Home extends Component {

    render() {
        let routes = (
            <Switch>
                <Route path="/home" exact component={Calorie} />
                <Redirect to="/home" />
            </Switch>
        );
        return (
            <div>
                <NavigationBar />
                {routes}
            </div>
        );
    }
}

export default Home;