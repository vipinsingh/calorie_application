import React, { Component } from 'react';
// import Logo from '../../../assets/images/biz2X-logo.svg';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import classes from './NavigationBar.module.css';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class NavigationBar extends Component {

    render() {
          return (
            <div>
                <Navbar bg="dark" expand="lg" variant="dark">
                    {/* <Navbar.Brand> <Link to="/projects" ><img src={Logo} className={classes.logoImg} alt="CalorieLogo"/></Link></Navbar.Brand> */}
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">

                        <Nav className="ml-auto">
                            <NavDropdown title={this.props.userData.userName} id="basic-nav-dropdown" className={classes.dropDownWidth}>
                                <NavDropdown.Item as={Link} to="/logout">Logout</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        userData : state.auth.userData
    };
};


export default connect(mapStateToProps)(NavigationBar);