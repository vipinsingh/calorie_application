import React,{useState,useEffect} from 'react'
import axios from '../../../axiosInstance';
import {Card} from 'react-bootstrap';
import Table from "../../../components/Table/Table";
import {CalorieFormControl} from "./CaloriFormConfig";
import { updateObject, checkValidity,resetObject,feedObject } from "../../../shared/utility";
import Modal from "../../../components/UI/Modal/Modal";
import DynamicForm from '../../../components/DynamicForm/DynamicForm';
import { connect } from "react-redux";



 const Calorie = ({user_id}) => {
    const [calories,setCalories] = useState([]);
    const [createCalorieForm,setCalorieForm]  = useState(CalorieFormControl);
    const [formIsValid,setformIsValid] = useState(false);
    const [showModal,setshowModal] = useState(false)
    const [submitLabel , setSubmitLabel] = useState(true);
    const [updatedId,setUpdatedId] = useState(0)
    const [calorieCount,setCalorieCount] = useState(0);

    useEffect(async ()=>{
        let data = await axios.get('/api/v1/calorie',{query: {user_id: user_id}});
        setCalories(data.data.data)
        setCalorieCount(totalCalories(data.data.data))
    },[])

    async function deleteClicked(id){
        await axios.delete(`/api/v1/calorie/${id}`);
        let updatedCalorie = calories.filter((value)=>{
            if(value.id != id) return true;
        })
        setCalories(updatedCalorie)
        setCalorieCount(totalCalories(updatedCalorie))
    }
    

    const updateClicked = async (id) => {
        setshowModal(!showModal);
        setSubmitLabel(false)
        const calorie = calories.filter((value)=>{
            if(id == value.id){
                return true
            }
        })
        setUpdatedId(id)
        updateObject(createCalorieForm,feedObject(createCalorieForm,calorie[0]))

    }

    const totalCalories = (calories) => {
        let sum = 0;
        for(let i=0;i<calories.length;i++){
            sum = sum + calories[i].calorie;
        }
        return sum;
    }

    
   const modalCloseHandler = () => {
        setshowModal(false);
    }

   const modalSubmitHandler = async (event) => {
        event.preventDefault();
        let mealData;
        let res;
        if(submitLabel){
            mealData = {
                user_id : user_id,
                meal        : createCalorieForm.meal.value,
                calorie : createCalorieForm.calorie.value
            };
            res = await axios.post('/api/v1/calorie',mealData);
            setCalories([...calories,{...res.data.data}])
            setCalorieCount(totalCalories([...calories,{...res.data.data}]))
        }else{
            mealData = {
                meal        : createCalorieForm.meal.value,
                calorie : createCalorieForm.calorie.value
            };
            await axios.patch(`/api/v1/calorie/${updatedId}`, mealData); 
            let data = await axios.get(`/api/v1/calorie/${updatedId}`);            
           let index = calories.findIndex((value)=>{
               if(value.id == updatedId){
                   return true;
               }
           })

           calories.splice(index,1)
           calories.splice(index,0,data.data.data)

           setCalories(calories)
           setCalorieCount(totalCalories(calories))

        } 
        updateObject(createCalorieForm,resetObject(createCalorieForm))
        setshowModal(!showModal)
    }

   const inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(createCalorieForm, {
            [controlName] : updateObject(createCalorieForm[controlName], {
                value : event.target.value,
                valid : checkValidity(event.target.value, createCalorieForm[controlName].validation),
                touched : true
            }) 
        });

        let formIsValid = true;

        for(let control in updatedControls) {
            formIsValid = updatedControls[control].valid && formIsValid;
        }

        setCalorieForm(updatedControls);
        setformIsValid(formIsValid)
    }

    const dynamicForm = <DynamicForm changed={inputChangedHandler} valid={formIsValid} controls={createCalorieForm}/>


    return (
    
        <div>
             <Modal
                    show={showModal}
                    handleClose={modalCloseHandler}
                    modalTitle= {submitLabel?"Create Calorie":"Edit Calorie"}
                    submitDisable={!formIsValid}
                    handleSubmit={modalSubmitHandler}
                    submitLabel = {submitLabel?"Create":"Edit"}
                >
                    <form>
                        {dynamicForm}
                    </form>
                </Modal>
            <button onClick={()=>{
                setshowModal(!showModal)
                setSubmitLabel(true)
            }}>Add Meal</button>
            <Card bg="light">
                <Card.Body>
                <Card.Title>
                <div className="float-left">Calories</div>
                </Card.Title>
                </Card.Body>
            </Card>
            <Table
                tableView='meal'
                tableData={calories}
                calorieCount = {calorieCount}
                attribute='true'
                deleteClicked = {deleteClicked}
                updateClicked = {updateClicked}
            />
            <div>
            </div>
            <p>Total Calories: {calorieCount}</p>
        </div>

        
    );
}

const mapStateToProps = state => {
    return {
        user_id : state.auth.userData.user_id
    };
};

export default connect(mapStateToProps)(Calorie);