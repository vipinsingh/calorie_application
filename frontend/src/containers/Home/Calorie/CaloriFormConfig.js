export const CalorieFormControl = {
    meal :  {
        elementType : 'input',
        elementConfig : {
            type : 'input',
            placeholder : 'Meal Name'
        },
        value : '',
        validation : {
            required : true,
            minLength : 3,
            maxLength : 100
        },
        valid : false,
        touched : false
    },
    calorie :  {
        elementType : 'input',
        elementConfig : {
            type : 'number',
            placeholder : 'Calorie No'
        },
        value : '',
        displayValue  :  'Number',
        validation : {
            required : true,
            minLength : 1,
            maxLength : 200
        },
        valid : false,
        touched : false
    }
}