
export const updateObject = (oldObject, updatedProps) => {
    return {
        ...oldObject,
        ...updatedProps
    };
};

export const resetObject = (oldObject) => {
    let keys = Object.keys(oldObject);
    for(let i=0;i<keys.length;i++){
        oldObject[keys[i]].value = '';
        oldObject[keys[i]].valid = false
        oldObject[keys[i]].touched = false
    } 
    return oldObject;
}

export const feedObject = (oldObject,obj)=>{
    let keys = Object.keys(oldObject);
    for(let i=0;i<keys.length;i++){
        oldObject[keys[i]].value = obj[keys[i]];
        oldObject[keys[i]].valid = true
        oldObject[keys[i]].touched = true
    } 
    return oldObject;
}


// export const insertObject = (oldObject) => {
//     let keys = Object.keys(oldObject);
//     for(let i=0;i<keys.length;i++){
//         oldObject[keys[i]].value = '';
//         oldObject[keys[i]].valid = false
//         oldObject[keys[i]].touched = false
//     } 
//     return oldObject;
// }

export const checkValidity = (value, rules) => {
    let isValid = true;

    if (!rules) {
        return true;
    }

    if (rules.required) {
        isValid = value.trim() !== '' && isValid;
    }

    if (rules.minLength) {
        isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
        isValid = value.length <= rules.maxLength && isValid;
    }

    if (rules.isEmail) {
        const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        isValid = pattern.test(value) && isValid
    }

    if (rules.isAlphaNum) {
        const pattern = /^[\d\sa-zA-Z]+$/;
        isValid = pattern.test(value) && isValid
    }

    if (rules.isNumeric) {
        const pattern = /^[\d-.]+$/;
        isValid = pattern.test(value) && isValid
    }

    if (rules.isNumericPositive) {
        const pattern = /^[\d.]+$/;
        isValid = pattern.test(value) && isValid
    }

    if (rules.isNumericComma) {
        const pattern = /^[\d,-.]+$/;
        isValid = pattern.test(value) && isValid
    }

    if (rules.isJson) {
        isValid = isValidJson(value) && isValid
    }

    if (rules.isApiKey) {
        const pattern = /^[a-z_A-Z]+$/;
        isValid = pattern.test(value) && isValid
    }

    return isValid;
}

const isValidJson = (str) => {
    try {
        JSON.parse(str);

        if (str.indexOf('{') === -1 || str.indexOf('"') === -1) {
            return false;
        }

        return true;
    } catch (error) {
        return false;
    }
}


export const getDateTimeFromISO = (ISOstring) => {
    const date = new Date(ISOstring);
    const year = date.getFullYear();
    let month = date.getMonth() + 1;
    let dt = date.getDate();
    const time = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) +
        ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) +
        ':' + (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());

    if (dt < 10) {
        dt = '0' + dt;
    }
    if (month < 10) {
        month = '0' + month;
    }

    return (month + '-' + dt + '-' + year + ' ' + time);
}
