import React from 'react';
import { Table } from 'react-bootstrap'; 
import { getDateTimeFromISO } from '../../shared/utility';

const table = (props) => {
    let colSpan = 4;
    let styleData = props.calorieCount>2000?{ color : 'red'}:{ color : 'green'}
    let tableData = (
        <tr onClick={props.createClicked}>
            <td colSpan={colSpan} style={{textAlign : 'center', color : 'blue'}}> Get Started, create your first {props.tableView} </td>
        </tr>
    );
    
    if(props.tableData && props.tableData.length > 0) {
        tableData = props.tableData.map(data => {
            return (
                <tr key={data.id}  style={styleData}>
                    <td>{data.meal}</td>
                    <td>{data.calorie}</td>
                    <td>{getDateTimeFromISO(data.created_at)}</td>
                    <td>{getDateTimeFromISO(data.updated_at)}</td>
                    <td><button onClick={() => props.deleteClicked(data.id)}>Delete</button></td>
                    <td><button onClick={() => props.updateClicked(data.id)}>Update</button></td>
                </tr>
            )
        })
    }

    if(props.error) {
        tableData = (
            <tr>
                <td colSpan={colSpan} style={{textAlign : 'center', color : 'red'}}>Unable to fetch details right now, please try later.</td>
            </tr>
        );
    }

    return (
        <Table striped bordered hover style={{width : '90%', marginLeft : '40px'}}>
            <thead>
                <tr>
                    <th>Meal</th>
                    <th>Calorie_No</th>
                    <th>Creation Date</th>
                    <th>Updation Date</th>
                    <th>Delete</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                {tableData}
            </tbody>
        </Table>
    );
}

export default table;