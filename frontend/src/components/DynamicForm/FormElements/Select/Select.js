import React from 'react';
import classes from './Select.module.css';

const select = (props) => {
    const inputClasses = [classes.SelectElement];

    if(props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push(classes.Invalid);
    }

    const inputElement = (
        <select
            className={inputClasses.join(' ')}
            value={props.value}
            onChange={props.changed}
        >
            {
                props.elementConfig.options.map(option => (
                    <option key={option.value} value={option.value}>
                        {option.displayValue}
                    </option>
                ))
            }
        </select>
    );

    return (
        <div className={classes.Select}>
            <label className={classes.Label}>{props.label}</label>
            {inputElement}
        </div>
    );
};

export default select;