import React from 'react';
import classes from './TextArea.module.css';

const textarea = (props) => {
    const inputClasses = [classes.TextAreaElement];

    if(props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push(classes.Invalid);
    }

    const inputElement = <textarea className={inputClasses.join(' ')} {...props.elementConfig} value={props.value} onChange={props.changed} />;

    return (
        <div className={classes.TextArea}>
            <label className={classes.Label}>{props.label}</label>
            {inputElement}
        </div>
    );
};

export default textarea;