import React, {Component} from 'react';
import {Alert} from 'react-bootstrap';

export default class AlertCard extends Component{

  constructor (props){
    super(props)

    this.state = {
      setShow: true
    }
  }
  
  onClose = () => {
    this.setState({
      setShow: false
    })
  }
  
  render(){
  
    if(this.state.setShow === true){
      return (
        <Alert variant={(this.props.isError === true) ? 'danger' : 'success' } onClose={this.onClose} dismissible>
        {(this.props.isError === true) && <Alert.Heading> Ooops!! </Alert.Heading>}
          
        <p>
          { (this.props.message) ? this.props.message : (this.props.isError === true) ? 'Something went wrong!!' : 'Successfull' }
        </p>
        </Alert>
      )
    }

    return null;
    
  }

}

