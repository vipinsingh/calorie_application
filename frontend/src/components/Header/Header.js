import React from 'react';
import classes from './Header.module.css';

const header = () => {
    return ( <div className = { classes.Header }>
        <div className= {classes.titleName}>Calorie Calculator</div>
        </div>
    );
}

export default header;